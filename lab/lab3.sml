datatype tokens = Nont of string | Term of string;
datatype lhs = Nonte of string;
(*
type rhs = tokens list;
type rule =  lhs*rhs;
type prod = rule list;
*)
val c = [(Nonte "S",[Term "a", Nont "S",Term "a"]),(Nonte "S",[])];   

fun f [] = [] 
| f ((a,ax)::ys) = a::(f ys)  ;
val temp=f c ;

fun x []=[]
| x ((a,ax)::ys)=ax @ (x ys);

val temp1=x c;

fun isolate [] = []
  | isolate (x::xs) = x::isolate(List.filter (fn y => y <> x) xs);
  
val nont=isolate temp;

val token = isolate temp1;

(*
signature ORD_KEY = sig
type ord_key
val compare : ord_key * ord_key -> order
end

structure Tokens_Key:ORD_KEY=struct
type ord_key= tokens
*)
(*
fun comp (x:tokens,y:tokens) = case (x,y) of 
    (Nont(x), Nont(y)) => if (x = y) then EQUAL else GREATER
    (Nont(x),Term(y)) => if (x = y) then EQUAL else GREATER;

*)
fun comp (x,y) = if (x = y) then EQUAL else GREATER;
(*
end;
*)
(*
fun str (x:tokens):string = x;
*)
structure Map = RedBlackMapFn (struct type ord_key = tokens val compare = comp end);
val nullable = Map.empty ;

fun initialising ( [] , nullable) = nullable 
| initialising( x::xs  , nullable ) = initialising( xs , Map.insert(nullable ,x ,false )) ;

val nullable =  initialising (token ,nullable );

structure MapProd = RedBlackMapFn (struct type ord_key = lhs val compare = comp end);
val productions = MapProd.empty;

fun filling ( [] , productions) = productions
| filling( (a,ax)::xs  ,productions) = filling( xs , MapProd.insert(productions ,a ,ax ));

val productions = filling( c , productions);

Map.listItemsi(nullable);























