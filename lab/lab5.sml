signature Instruction =
sig
    type inst
    val use : inst -> AtomSet.set (* Get the use set of an instruction *)
    val def : inst -> AtomSet.set (* Get the def set of an instruction *)
end
structure node : Instruction =
struct
      type inst             = Atom.atom * AtomSet.set     
      fun use (l, r)        = r    
      fun def (l, r)        = AtomSet.singleton(l)
end;
fun toAtom (lhs, rhs)   = (Atom.atom(lhs), AtomSet.fromList(List.map (Atom.atom) rhs)) 
fun lToAtom (x)         = List.map (toAtom) x

fun toString(x)         = List.map (Atom.toString) (AtomSet.listItems(x))

structure Key = struct
    type ord_key = int
    fun compare(x,y)=Int.compare(x,y)
    end;

structure MapFun=RedBlackMapFn(Key);
structure SetFun = RedBlackSetFn (Key);
val inMap=MapFun.empty;
val outMap=MapFun.empty;
val useMap=MapFun.empty;
val defMap=MapFun.empty;
val successor=MapFun.empty;
val predecessor=MapFun.empty;


functor InOut(X : Instruction)= struct

fun newnode (x,predecessor,successor) = (MapFun.insert(predecessor,x,SetFun.empty), MapFun.insert(successor,x,SetFun.empty));

fun addedge (x,y,predecessor,successor)=(MapFun.insert(predecessor,y,SetFun.add(MapFun.lookup(predecessor,y),x)),MapFun.insert(successor,x,SetFun.add(MapFun.lookup(successor,x),y))); 

fun node (x,(predecessor,successor))= case x of 
0=>(predecessor,successor)
|x=>node(x-1,newnode(x,predecessor,successor));

fun edge (x,(predecessor,successor)) = case x of
[]=>(predecessor,successor)
|((a,b)::nil)=>addedge(a,b,predecessor,successor)
|((a,b)::xs)=>edge(xs,addedge(a,b,predecessor,successor));

fun init(function, [], p)    = function
                                             | init(function, x::xs, p) = init(MapFun.insert(function, x, p), xs, p);
   
fun useDef(function, [], instruct, f)    = function
			                                 | useDef(function, x::xs, instruct, f) = 
			                             let
			                                val function = MapFun.insert(function, x, f(MapFun.lookup(instruct, x)))
			                              in
			                                useDef(function, xs, instruct, f)
			                               end;

fun c (x,[])=true
|c (x,(a::nil))=if x = a then false else true
|c (x,(a::b))= if x = a then false else c(x,b); 

fun basic (predecessor,successor,[],bb,flagPred,flagSucc) = bb 
|basic (predecessor,successor,[x],bb,flagPred,flagSucc) = 
	if (c(x,bb)) andalso SetFun.numItems(MapFun.lookup(predecessor,x)) = 1 then 
	(   if (c(x,bb)) andalso SetFun.numItems(MapFun.lookup(successor,x)) = 1 then 
			basic (predecessor,successor,SetFun.listItems(MapFun.lookup(predecessor,x)),basic (predecessor,successor,SetFun.listItems(MapFun.lookup(successor,x)),bb @ [x],false,true),true,false)
		else
		(
		         if  SetFun.numItems(MapFun.lookup(successor,x)) > 1 then 
		         (
		         	if flagSucc then 
		         		basic (predecessor,successor,SetFun.listItems(MapFun.lookup(predecessor,x)),bb @ [x],true,false ) 
         			else bb	
     			)
		         else basic (predecessor,successor,SetFun.listItems(MapFun.lookup(predecessor,x)),bb @ [x],true,false )
		)
	)
	else 
	(    
		if SetFun.numItems(MapFun.lookup(predecessor,x)) > 1 then 	
		(
			if flagPred then basic (predecessor,successor,SetFun.listItems(MapFun.lookup(successor,x)),bb @ [x],false,true ) 
			else bb
		)

		else 
		(   
			if (c(x,bb)) andalso SetFun.numItems(MapFun.lookup(successor,x)) =1 then basic (predecessor,successor,SetFun.listItems(MapFun.lookup(successor,x)),bb @ [x],false,true) 
		    else bb@[x] 
		)
	)
fun equal(sA, sB, [])    = true
                                            | equal(sA, sB, x::xs) = if(AtomSet.equal(MapFun.lookup(sA, x), MapFun.lookup(sB, x)) = false)   then false else equal(sA, sB, xs)
fun isolate [] = []
  | isolate (x::xs) = x::isolate(List.filter (fn y => y <> x) xs)
val z=isolate(basic (predecessor, successor,[3],[],true,true));

fun listCal(1) = [isolate(basic (predecessor, successor,[1],[],true,true))]
| listCal(x) = (  isolate(basic (predecessor, successor,[x],[],true,true)) ::listCal(x-1) );
(*


end;

*)

fun concate(function, [])    = AtomSet.empty
       | concate(function, x::xs) = AtomSet.union(MapFun.lookup(function, x), concate(function, xs));
    
end;



