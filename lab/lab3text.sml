signature ORD = sig
type t
val le : t -> t -> bool
    end

structure IntOrd : ORD = struct
type t = int
fun le u v = u>v;
end

structure RealOrd : ORD = struct
type t = real
fun le (u : real) v = u>v;
end

functor Qsort(X : ORD) = struct
fun sort [] = [] 
        | sort (x::xs) = let 
                                val(l,u) = List.partition(X.le x) xs       
                         in
                                sort l @ [x] @ sort u
                         end;
end

structure IntSort = Qsort(IntOrd);

IntSort.sort [3,3,4,1,8,3,5,3]


